# Vue 3 + Vite + ElementPlus + Koa2

Ned 上手 Vue3 的第一个练手项目，也是第一个自己尝试写后端的项目。

Vue2 ---> Vue3 升级 ing

node 后端学习 0 ---> 1 学习 ing

目前进度 ✔：

- 路由封装完成
- axios 封装完成
- storage 封装完成
- 首页布局结构搭建完成
- 导航栏菜单结构搭建完成
- 菜单交互,递归组件实现完成
- 用户管理列表实现完成
- 用户删除（含批量）、新增、编辑实现完成
- 日期格式化工具函数封装完成
- 全局配置基本完成
- jwt token
- 后端日志封装完成（采用 log4js）
- 登陆前后台逻辑（测试完成）
- 用户管理接口实现完成（测试完成）
- 用户删除接口实现完成（测试完成）
- 用户编辑接口实现完成（测试完成）
- 用户新增接口实现完成（待测试）
- 后端一些工具函数的封装
- 数据库表结构设计完成（MongoDB）

接下来完成 🎁：

- 菜单模块的前后端实现
- 要期末了，偶尔要摸摸鱼去看别的了 👻
