/**
 * 环境配置封装
 * 默认是生产环境 prod
 * 如果有值就赋值到env变量上，当做当前的环境变量
 */

const env = import.meta.env.MODE || 'prod'
const EnvConfig = {
    dev:{
        baseApi:'/api',
        mockApi:'https://www.fastmock.site/mock/950bc5af398f16028f00a8afa77e255b/api'
    },
    test:{
        baseApi:'//test.wangez.site/api',
        mockApi:'https://www.fastmock.site/mock/950bc5af398f16028f00a8afa77e255b/api'
    },
    prod:{
        baseApi:'//wangez.site/api',
        mockApi:'https://www.fastmock.site/mock/950bc5af398f16028f00a8afa77e255b/api'
    }
}
export default {
    namespace:'manager',
    env:env,
    mock:true,
    ...EnvConfig[env]
}