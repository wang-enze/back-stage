/**
 * api管理
 */
import request from './../utils/request'
export default {
    login(params){
        return    request({
            method:'POST',
            url:'/users/login',
            data:params,
            mock:false
        })
    },
    noticeCount(params){
        return    request({
            method:'GET',
            url:'/leave/count',
            data:{},
            mock:true
        })
    },
    getMenuList(params){
        return    request({
            method:'GET',
            url:'/menu/list',
            data:{},
            mock:true
        })
    },
    getUserList(params){
        return    request({
            method:'GET',
            url:'/users/list',
            data:params,
            mock:false
        })
    },
    userDel(params){
        return    request({
            method:'POST',
            url:'/users/delete',
            data:params,
            mock:false
        })
    },
    getRoleList(params){
        return    request({
            method:'GET',
            url:'/roles/allList',
            data:params,
            // mock:true
        })     
    },
    getDeptList(params){
        return    request({
            method:'GET',
            url:'/dept/list',
            data:params,
            // mock:true
        })     
    },
    userSubmit(params){
        return    request({
            method:'POST',
            url:'/users/operate',
            data:params,
            mock:false
        })     
    },
    menuSubmit(params){
        return    request({
            method:'POST',
            url:'/menu/operate',
            data:params,
            mock:true
        })     
    }
}